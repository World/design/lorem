// SPDX-License-Identifier: GPL-3.0-or-later
#![forbid(unsafe_code)]
mod application;
#[rustfmt::skip]
mod config;
mod window;

use application::Application;
use config::{APP_NAME, GETTEXT_PACKAGE, LOCALEDIR, RESOURCES_FILE};
use gettextrs::LocaleCategory;
use gtk::{gio, glib};

fn main() -> glib::ExitCode {
    // Initialize logger
    env_logger::init();

    // Prepare i18n
    gettextrs::setlocale(LocaleCategory::LcAll, "");
    gettextrs::bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR).expect("Unable to bind the text domain");
    gettextrs::textdomain(GETTEXT_PACKAGE).expect("Unable to switch to the text domain");

    glib::set_application_name(APP_NAME);

    let res = gio::Resource::load(RESOURCES_FILE).expect("Could not load gresource file");
    gio::resources_register(&res);

    let app = Application::new();

    app.run()
}

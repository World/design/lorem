# Italian translation for lorem.
# Copyright (C) 2023 lorem's COPYRIGHT HOLDER
# This file is distributed under the same license as the lorem package.
# Davide Ferracin <davide.ferracin@protonmail.com>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: lorem master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/design/lorem/issues\n"
"POT-Creation-Date: 2023-04-03 20:18+0000\n"
"PO-Revision-Date: 2023-04-17 10:33+0200\n"
"Last-Translator: Davide Ferracin <davide.ferracin@protonmail.com>\n"
"Language-Team: Italian <gnome-it-list@gnome.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: data/org.gnome.design.Lorem.desktop.in.in:3
#: data/org.gnome.design.Lorem.metainfo.xml.in.in:7
msgid "Lorem"
msgstr "Lorem"

#: data/org.gnome.design.Lorem.desktop.in.in:4
#: data/org.gnome.design.Lorem.metainfo.xml.in.in:8 src/application.rs:109
msgid "Generate placeholder text"
msgstr "Genera testo riempitivo"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.design.Lorem.desktop.in.in:10
msgid "placeholder;"
msgstr "riempitivo;"

#: data/org.gnome.design.Lorem.gschema.xml.in:10
#: data/org.gnome.design.Lorem.gschema.xml.in:11
msgid "Default window width"
msgstr "Larghezza predefinita della finestra"

#: data/org.gnome.design.Lorem.gschema.xml.in:15
#: data/org.gnome.design.Lorem.gschema.xml.in:16
msgid "Default window height"
msgstr "Altezza predefinita della finestra"

#: data/org.gnome.design.Lorem.gschema.xml.in:20
msgid "Default window maximized behaviour"
msgstr "Comportamento di massimizzazione predefinito della finestra"

#: data/org.gnome.design.Lorem.metainfo.xml.in.in:10
msgid "Simple app to generate the well-known Lorem Ipsum placeholder text."
msgstr "Una semplice app per generare il ben noto testo riempitivo Lorem Ipsum."

#: data/org.gnome.design.Lorem.metainfo.xml.in.in:15
msgid "Main Window"
msgstr "Finestra principale"

#: data/org.gnome.design.Lorem.metainfo.xml.in.in:53
msgid "Maximiliano Sandoval"
msgstr "Maximiliano Sandoval"

#: data/resources/ui/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Generali"

#: data/resources/ui/shortcuts.ui:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Mostra scorciatoie"

#: data/resources/ui/shortcuts.ui:20
msgctxt "shortcut window"
msgid "Copy"
msgstr "Copia"

#: data/resources/ui/shortcuts.ui:26
msgctxt "shortcut window"
msgid "Main Menu"
msgstr "Menù principale"

#: data/resources/ui/shortcuts.ui:32
msgctxt "shortcut window"
msgid "Quit"
msgstr "Esce"

#: data/resources/ui/window.ui:6
msgid "_Keyboard Shortcuts"
msgstr "_Scorciatoie da tastiera"

#: data/resources/ui/window.ui:9
msgid "_About Lorem"
msgstr "_Informazioni su Lorem"

#: data/resources/ui/window.ui:38
msgid "Amount"
msgstr "Quantità"

#: data/resources/ui/window.ui:46
msgid "Type"
msgstr "Tipo"

#: data/resources/ui/window.ui:67
msgid "Main Menu"
msgstr "Menù principale"

#: data/resources/ui/window.ui:75
msgid "Copy"
msgstr "Copia"

#: src/application.rs:115
msgid "translator-credits"
msgstr "Davide Ferracin <davide.ferracin@protonmail.com>"

#: src/window.rs:25
msgid "Paragraphs"
msgstr "Paragrafi"

#: src/window.rs:26
msgid "Words"
msgstr "Parole"

#: src/window.rs:259
msgid "Selection copied"
msgstr "Selezione copiata"

#: src/window.rs:264
msgid "Copied"
msgstr "Copiato"

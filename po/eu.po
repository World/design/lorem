# Basque translation for lorem.
# Copyright (C) 2021 lorem's COPYRIGHT HOLDER
# This file is distributed under the same license as the lorem package.
# Asier Sarasua Garmendia <asiersarasua@ni.eus>, 2021, 2022, 2023.
#
msgid ""
msgstr "Project-Id-Version: lorem master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/design/lorem/issues\n"
"POT-Creation-Date: 2023-05-22 11:13+0000\n"
"PO-Revision-Date: 2023-09-02 18:08+0000\n"
"Last-Translator: Asier Sarasua Garmendia <asiersarasua@ni.eus>\n"
"Language-Team: Basque <librezale@librezale.eus>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/org.gnome.design.Lorem.desktop.in.in:3
#: data/org.gnome.design.Lorem.metainfo.xml.in.in:7
msgid "Lorem"
msgstr "Lorem"

#: data/org.gnome.design.Lorem.desktop.in.in:4
#: data/org.gnome.design.Lorem.metainfo.xml.in.in:8 src/application.rs:109
msgid "Generate placeholder text"
msgstr "Sortu leku-marka moduko testua"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.design.Lorem.desktop.in.in:10
msgid "placeholder;"
msgstr "leku-marka;"

#: data/org.gnome.design.Lorem.gschema.xml.in:10
#: data/org.gnome.design.Lorem.gschema.xml.in:11
msgid "Default window width"
msgstr "Leihoaren zabalera lehenetsia"

#: data/org.gnome.design.Lorem.gschema.xml.in:15
#: data/org.gnome.design.Lorem.gschema.xml.in:16
msgid "Default window height"
msgstr "Leihoaren altuera lehenetsia"

#: data/org.gnome.design.Lorem.gschema.xml.in:20
msgid "Default window maximized behavior"
msgstr "Maximizatutako leihoaren portaera lehenetsia"

#: data/org.gnome.design.Lorem.metainfo.xml.in.in:10
msgid "Simple app to generate the well-known Lorem Ipsum placeholder text."
msgstr "Leku-marka modura erabiltzeko Lorem Ipsum testua sortzeko aplikazio sinplea."

#: data/org.gnome.design.Lorem.metainfo.xml.in.in:15
msgid "Main Window"
msgstr "Leiho nagusia"

#: data/org.gnome.design.Lorem.metainfo.xml.in.in:53
msgid "Maximiliano Sandoval"
msgstr "Maximiliano Sandoval"

#: data/resources/ui/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Orokorra"

#: data/resources/ui/shortcuts.ui:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Erakutsi lasterbideak"

#: data/resources/ui/shortcuts.ui:20
msgctxt "shortcut window"
msgid "Copy"
msgstr "Kopiatu"

#: data/resources/ui/shortcuts.ui:26
msgctxt "shortcut window"
msgid "Main Menu"
msgstr "Menu nagusia"

#: data/resources/ui/shortcuts.ui:32
msgctxt "shortcut window"
msgid "Quit"
msgstr "Irten"

#: data/resources/ui/window.ui:6
msgid "_Keyboard Shortcuts"
msgstr "Las_ter-teklak"

#: data/resources/ui/window.ui:9
msgid "_About Lorem"
msgstr "Lorem aplikazioari _buruz"

#: data/resources/ui/window.ui:38
msgid "Amount"
msgstr "Kantitatea"

#: data/resources/ui/window.ui:46
msgid "Type"
msgstr "Mota"

#: data/resources/ui/window.ui:67
msgid "Main Menu"
msgstr "Menu nagusia"

#: data/resources/ui/window.ui:75
msgid "Copy"
msgstr "Kopiatu"

#: src/application.rs:115
msgid "translator-credits"
msgstr "translator-credits"

#: src/window.rs:25
msgid "Paragraphs"
msgstr "Paragrafoak"

#: src/window.rs:26
msgid "Words"
msgstr "Hitzak"

#: src/window.rs:258
msgid "Selection copied"
msgstr "Hautapena kopiatu da"

#: src/window.rs:263
msgid "Copied"
msgstr "Kopiatua"

#~ msgid "Menu"
#~ msgstr "Menua"

#~ msgid "Gnome;GTK;"
#~ msgstr "Gnome;GTK;"

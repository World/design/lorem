<a href="https://flathub.org/apps/details/org.gnome.design.Lorem">
<img src="https://flathub.org/api/badge?svg&locale=en&light" />
</a>

# Lorem

<img src="https://gitlab.gnome.org/World/design/lorem/raw/master/data/icons/org.gnome.design.Lorem.svg" width="128" height="128" />
<p>Simple app to generate the Lorem Ipsum placeholder text.</p>

## Screenshots

<div align="center">
![screenshot](https://gitlab.gnome.org/World/design/lorem/raw/master/screenshots/main_window.png)
</div>

## Hack on Lorem

To build the development version of Lorem and hack on the code see the [general
guide](https://welcome.gnome.org/en/app/Lorem/#getting-the-app-to-build) for
building GNOME apps with Flatpak and GNOME Builder.

## Translations

Helping to translate Lorem or add support to a new language is very welcome.
You can find everything you need at: [l10n.gnome.org/module/lorem/](https://l10n.gnome.org/module/lorem/)

## Code Of Conduct

This project follows the [GNOME Code of Conduct](https://conduct.gnome.org/).
